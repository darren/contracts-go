package main

import (
	"encoding/json"
	"fmt"
	"strconv"
	"strings"

	"chainmaker.org/chainmaker/common/v2/crypto"
	"chainmaker.org/chainmaker/contract-sdk-go/v2/pb/protogo"
	"chainmaker.org/chainmaker/contract-sdk-go/v2/sdk"
	"chainmaker.org/chainmaker/contract-utils/address"
	"chainmaker.org/chainmaker/contract-utils/safemath"
)

const (
	tokenURIMapName   = "tokenURIs"
	ownerTokenMapName = "ownerTokenIds"
)

func (c *ERC721Contract) nftTransferFrom(from, to string, tokenId *safemath.SafeUint256,
	data []byte) protogo.Response {
	sender, err := sdk.Instance.Sender()
	if err != nil {
		return sdk.Error(fmt.Sprintf("get sender failed, err:%s", err))
	}
	isApprovedOrOwner, err := c.isApprovedOrOwner(sender, tokenId)
	if err != nil {
		return sdk.Error(fmt.Sprintf("check isApprovedOrOwner failed, err:%s", err))
	}
	if !isApprovedOrOwner {
		return sdk.Error("ERC721: caller is not token owner or approved")
	}
	if from == "" {
		from = sender
	}
	return c.nftTransfer(from, to, tokenId)
}

func (c *ERC721Contract) nftTransfer(from, to string, tokenId *safemath.SafeUint256) protogo.Response {
	response := c.ownerOf(tokenId)
	if response.Status != sdk.OK {
		return response
	}
	owner := string(response.Payload)
	if owner != from {
		return sdk.Error("ERC721: transfer from incorrect owner")
	}

	if !address.IsValidAddress(to) {
		return sdk.Error("ERC20: transfer to the invalid address")
	}

	if address.IsZeroAddress(to) {
		return sdk.Error("ERC20: transfer to the zero address")
	}
	// delete token approve
	tokenApproveInfo, err := sdk.NewStoreMap(tokenApproveMapName, 1, crypto.HASH_TYPE_SHA256)
	if err != nil {
		return sdk.Error(fmt.Sprintf("New storeMap of balanceInfo failed, err:%s", err))
	}
	if err := tokenApproveInfo.Del([]string{tokenId.ToString()}); err != nil {
		return sdk.Error(fmt.Sprintf("delete token approve failed, err:%s", err))
	}

	// update "from" balance count
	if err := c.decreaseTokenCountByOne(from); err != nil {
		return sdk.Error(err.Error())
	}
	// del "from" account tokenID list
	if err := c.delOwnerTokenListByTokenID(from, tokenId); err != nil {
		return sdk.Error(err.Error())
	}

	// update "to" balance count
	if err := c.increaseTokenCountByOne(to); err != nil {
		return sdk.Error(err.Error())
	}
	// update token owner
	if err := c.setTokenOwner(to, tokenId); err != nil {
		return sdk.Error(err.Error())
	}
	// update "to" account tokenID list
	if err := c.setOwnerTokenList(to, tokenId); err != nil {
		return sdk.Error(err.Error())
	}

	sdk.Instance.EmitEvent("transfer", []string{from, to, tokenId.ToString()})
	return sdk.Success([]byte("transfer success"))
}

func (c *ERC721Contract) setOwnerTokenList(account string, tokenId *safemath.SafeUint256) error {
	ownerTokenListInfo, err := sdk.NewStoreMap(ownerTokenMapName, 1, crypto.HASH_TYPE_SHA256)
	if err != nil {
		return fmt.Errorf("new storeMap of tokenOwner failed, err:%s", err)
	}

	tokenIdsBytes, err := ownerTokenListInfo.Get([]string{account})
	if err != nil {
		return fmt.Errorf("get ownerTokenListInfo err:%v", err)
	}
	var newTokenIds string
	tokenIdsStr := string(tokenIdsBytes)
	if tokenIdsStr == "" {
		newTokenIds = tokenId.ToString()
	} else {
		newTokenIds = fmt.Sprintf("%s;%s", tokenIdsStr, tokenId.ToString())
	}

	if err := ownerTokenListInfo.Set([]string{account}, []byte(newTokenIds)); err != nil {
		return fmt.Errorf("set ownerTokenLists failed, err:%v", err)
	}

	return nil
}

func (c *ERC721Contract) delOwnerTokenListByTokenID(account string, tokenId *safemath.SafeUint256) error {
	ownerTokenListInfo, err := sdk.NewStoreMap(ownerTokenMapName, 1, crypto.HASH_TYPE_SHA256)
	if err != nil {
		return fmt.Errorf("new storeMap of tokenOwner failed, err:%s", err)
	}

	tokenIdsBytes, err := ownerTokenListInfo.Get([]string{account})
	if err != nil {
		return fmt.Errorf("get ownerTokenListInfo err:%v", err)
	}

	tokenIdsStr := string(tokenIdsBytes)
	if tokenIdsStr == "" {
		return nil
	}
	var newTokenIds string
	for _, v := range strings.Split(tokenIdsStr, ";") {
		if v != tokenId.ToString() {
			newTokenIds += v + ";"
		}
	}
	newTokenIds = strings.TrimSuffix(newTokenIds, ";")
	if err := ownerTokenListInfo.Set([]string{account}, []byte(newTokenIds)); err != nil {
		return fmt.Errorf("set ownerTokenLists failed, err:%v", err)
	}
	return nil
}

func (c *ERC721Contract) mintForMyself(tokenURI string) protogo.Response {
	sender, err := sdk.Instance.Sender()
	if err != nil {
		return sdk.Error(fmt.Sprintf("get sender failed, err:%s", err))
	}

	if err := c.increaseTokenCountByOne(sender); err != nil {
		return sdk.Error(err.Error())
	}

	tokenID, err := c.generateTokenID()
	if err != nil {
		return sdk.Error("generate tokenID failed,err:" + err.Error())
	}

	tokenId, ok := safemath.ParseSafeUint256(strconv.Itoa(tokenID))
	if !ok {
		return sdk.Error("Parse tokenId failed")
	}

	if err := c.setTokenOwner(sender, tokenId); err != nil {
		return sdk.Error(err.Error())
	}

	if err := c.setTokenURI(tokenURI, tokenId); err != nil {
		return sdk.Error(err.Error())
	}

	if err := c.setOwnerTokenList(sender, tokenId); err != nil {
		return sdk.Error(err.Error())
	}

	return sdk.Success([]byte("mint for myself success "))
}

func (c *ERC721Contract) setTokenURI(tokenURI string, tokenId *safemath.SafeUint256) error {
	tokenURIInfo, err := sdk.NewStoreMap(tokenURIMapName, 1, crypto.HASH_TYPE_SHA256)
	if err != nil {
		return fmt.Errorf("new storeMap of tokenURI failed, err:%s", err)
	}

	if err := tokenURIInfo.Set([]string{tokenId.ToString()}, []byte(tokenURI)); err != nil {
		return fmt.Errorf("set token uri failed, err:%s", err)
	}
	return nil
}

func (c *ERC721Contract) generateTokenID() (int, error) {
	tokenOwnerInfo, err := sdk.NewStoreMap(tokenOwnerMapName, 1, crypto.HASH_TYPE_SHA256)
	if err != nil {
		return 0, err
	}

	iterator, err := tokenOwnerInfo.NewStoreMapIteratorPrefixWithKey(nil)
	if err != nil {
		return 0, err
	}
	count := 0
	for iterator.HasNext() {
		count++
	}
	return count, nil
}

// NFT model
type NFT struct {
	TokenID  string `json:"token_id"`
	TokenURI string `json:"token_uri"`
}

func (c *ERC721Contract) listNFT(account string) protogo.Response {
	if account == "" {
		sender, err := sdk.Instance.Sender()
		if err != nil {
			return sdk.Error(fmt.Sprintf("get sender failed, err:%s", err))
		}
		account = sender
	}
	if !address.IsValidAddress(account) {
		return sdk.Error("invalid address")
	}

	if address.IsZeroAddress(account) {
		return sdk.Error("ERC721:can not list the zero address's nft")
	}

	results, err := c.getOwnerTokenList(account)
	if err != nil {
		return sdk.Error(err.Error())
	}
	nftBytes, err := json.Marshal(&results)
	if err != nil {
		return sdk.Error("parse nft info data failed, err:" + err.Error())
	}
	return sdk.Success(nftBytes)
}

func (c *ERC721Contract) getOwnerTokenList(account string) ([]NFT, error) {
	ownerTokenListInfo, err := sdk.NewStoreMap(ownerTokenMapName, 1, crypto.HASH_TYPE_SHA256)
	if err != nil {
		return nil, fmt.Errorf("new storeMap of tokenOwner failed, err:%s", err)
	}

	tokenIdsBytes, err := ownerTokenListInfo.Get([]string{account})
	if err != nil {
		return nil, fmt.Errorf("get ownerTokenListInfo,err:%v", err)
	}

	results := make([]NFT, 0)
	for _, v := range strings.Split(string(tokenIdsBytes), ";") {
		tokenId, ok := safemath.ParseSafeUint256(v)
		if !ok {
			return nil, fmt.Errorf("parse tokenId failed,%v", ok)
		}
		tokenURI, err := c.getTokenURI(tokenId)
		if err != nil {
			return nil, fmt.Errorf("getTokenURI failed,err:%v", err)
		}
		results = append(results, NFT{
			TokenID:  v,
			TokenURI: tokenURI,
		})
	}
	return results, nil
}

func (c *ERC721Contract) getTokenURI(tokenId *safemath.SafeUint256) (string, error) {
	tokenURIInfo, err := sdk.NewStoreMap(tokenURIMapName, 1, crypto.HASH_TYPE_SHA256)
	if err != nil {
		return "", fmt.Errorf("new storeMap of tokenURI failed, err:%s", err)
	}

	tokenURIBytes, err := tokenURIInfo.Get([]string{tokenId.ToString()})
	if err != nil {
		return "", fmt.Errorf("get tokenURI failed, err:%v", err)
	}

	return string(tokenURIBytes), nil
}
