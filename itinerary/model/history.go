package model

type HistoryValue struct {
	Field       string      `json:"field"`
	Value       interface{} `json:"value"`
	TxId        string      `json:"txId"`
	Timestamp   string      `json:"timestamp"`
	BlockHeight int         `json:"blockHeight"`
	Key         string      `json:"key"`
}

type HistoryValues []HistoryValue

// Len 重写 Len() 方法
func (h HistoryValues) Len() int {
	return len(h)
}

// Swap 重写 Swap() 方法
func (h HistoryValues) Swap(i, j int) {
	h[i], h[j] = h[j], h[i]
}

// Less 重写 Less() 方法
// 使用不同字段进行对比，并依此排序，这里用了Age，可以更换成CreatedAt进行排序的
// 使用 < 从大到小排序，使用 > 从小到大排序
func (h HistoryValues) Less(i, j int) bool {
	return h[j].BlockHeight < h[i].BlockHeight
}
