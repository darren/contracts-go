package model

// Itinerary 行程定义，包含所在公网IP、网络运营商、国家省份城市、经纬度坐标、所在时区
type Itinerary struct {
	IP       string  `json:"ip"`
	City     string  `json:"city"`
	Region   string  `json:"region"`
	Country  string  `json:"country"`
	Loc      string  `json:"loc"`
	Org      string  `json:"org"`
	Timezone string  `json:"timezone"`
	Asn      Asn     `json:"asn"`
	Company  Company `json:"company"`
	Privacy  Privacy `json:"privacy"`
	Abuse    Abuse   `json:"abuse"`
	Domains  Domains `json:"domains"`
}

type Asn struct {
	Asn    string `json:"asn"`
	Name   string `json:"name"`
	Domain string `json:"domain"`
	Route  string `json:"route"`
	Type   string `json:"type"`
}
type Company struct {
	Name   string `json:"name"`
	Domain string `json:"domain"`
	Type   string `json:"type"`
}
type Privacy struct {
	Vpn     bool   `json:"vpn"`
	Proxy   bool   `json:"proxy"`
	Tor     bool   `json:"tor"`
	Relay   bool   `json:"relay"`
	Hosting bool   `json:"hosting"`
	Service string `json:"service"`
}
type Abuse struct {
	Address string `json:"address"`
	Country string `json:"country"`
	Email   string `json:"email"`
	Name    string `json:"name"`
	Network string `json:"network"`
	Phone   string `json:"phone"`
}
type Domains struct {
	Total   int           `json:"total"`
	Domains []interface{} `json:"domains"`
}
