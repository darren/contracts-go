package tools

import (
	"bytes"
	"errors"
	"fmt"
	"io"
	"net"
	"net/http"
	"os"
	"strconv"
	"strings"
)

type IPInfo struct {
	Code int `json:"code"`
	Data IP  `json:"data"`
}

type IP struct {
	Country   string `json:"country"`
	CountryId string `json:"country_id"`
	Area      string `json:"area"`
	AreaId    string `json:"area_id"`
	Region    string `json:"region"`
	RegionId  string `json:"region_id"`
	City      string `json:"city"`
	CityId    string `json:"city_id"`
	Isp       string `json:"isp"`
}

/*
*
公网IP查询：
http://site.ip138.com/
http://myip.ipip.net
https://myexternalip.com/raw/
https://www.eskysky.com/web/getip/
https://ip.cn/
https://ip.gs/
https://ipw.cn/
https://ifconfig.me/
https://ifconfig.io/
https://ifconfig.co/
https://ipinfo.io/
http://www.net.cn/static/customercare/yourip.asp
http://www.cip.cc/
https://icanhazip.com/
http://ipchaxun.sougongju.com/
https://ip.tool.lu/
https://api.ipify.org/
https://www.ipify.org/
http://whois.pconline.com.cn/
http://httpbin.org/ip
http://pv.sohu.com/cityjson
https://ip.tool.chinaz.com/
http://ip.zhenhaotv.com/
http://members.3322.org/dyndns/getip
https://ident.me/
https://ipecho.net/plain
http://whatismyip.akamai.com/
https://chaipip.com/aiwen.html
https://www.ipip.net/ip.html
https://inet-ip.info/
https://myip.dnsomatic.com/
https://tnx.nl/ip
*/
func main() {

	serverUrl := []string{
		"https://myexternalip.com/raw",
		"https://www.eskysky.com/web/getip/",
		"https://4.ipw.cn/",
		"https://ip.gs/",
		"https://ident.me/",
		"https://ifconfig.me",
		"https://icanhazip.com",
		"https://ipecho.net/plain",
		"https://tnx.nl/ip",
	}

	externalIp := ""
	for idx, server := range serverUrl {
		externalIp = getExternal(server)
		fmt.Printf("%d: %s, server=%s\n", idx, strings.TrimSpace(externalIp), server)
		if externalIp != "" {
			GetLocationByIp(externalIp)
		}
	}

	externalIp = strings.Replace(externalIp, "\n", "", -1)
	fmt.Println("externalIp: ", externalIp)

	fmt.Println("------GetIntranetIp------")

	GetIntranetIp()
	fmt.Println("------inetAton------")

	ipInt := inetAton(net.ParseIP(externalIp))
	fmt.Println("ipInt:", ipInt)

	ipNet := inetNtoa(ipInt)
	fmt.Println("ipNet:", ipNet)

	isBetween := IpBetween(net.ParseIP("0.0.0.0"), net.ParseIP("255.255.255.255"), net.ParseIP(externalIp))
	fmt.Println("isBetween: ", isBetween)

	isPublicIp := IsPublicIP(net.ParseIP(externalIp))
	fmt.Println("isPublicIp: ", isPublicIp)

	isPublicIp = IsPublicIP(net.ParseIP("169.254.85.131"))
	fmt.Println("isPublicIp: ", isPublicIp)

	fmt.Println("GetPublicIP: ", GetPublicIP())

	boundIP, _ := GetOutBoundIP()
	fmt.Println("GetOutBoundIP: ", boundIP)

	macAddr := GetMacAddr()
	fmt.Println("macAddr: ", macAddr)

	exip, _ := externalIP()
	fmt.Println("externalIP: ", exip)
}

func GetIpInfo() string {
	// curl https://ipinfo.io?token=8db4b68c584a81
	params := make(map[string]string, 0)
	params["token"] = "8db4b68c584a81"
	res, err := NewHttpRequest().Get("https://ipinfo.io?", nil, params)
	if err != nil {
		panic(err)
	}
	fmt.Println(res)
	return res
}

// GetLocationById 3. 普通IP定位: 根据IP定位来获取大致位置
// GET请求  https://api.map.baidu.com/location/ip?ak=您的AK&ip=您的IP&coor=bd09ll
// ak: 7F9WjSEIrXEVHSlK1CTH9eptVxx8lfL6
// sk: NT52kOLUNwItGodAYpuVAdfxBYPR8PeM
func GetLocationByIp(publicIp string) string {
	ak := "7F9WjSEIrXEVHSlK1CTH9eptVxx8lfL6"
	url := "https://api.map.baidu.com/location/ip?"
	req := NewHttpRequest()

	params := make(map[string]string, 0)
	params["ip"] = publicIp
	params["coor"] = "bd09ll"
	params["ak"] = ak
	params["sn"] = "cb98eb16971cef85d1b8c111b1d5b7a2"

	res, err := req.Get(url, nil, params)
	if err != nil {
		panic(err)
	}
	fmt.Println(res)
	return ""
}

func getExternal(serverUrl string) string {
	resp, err := http.Get(serverUrl)
	if err != nil {
		return ""
	}
	defer resp.Body.Close()
	content, _ := io.ReadAll(resp.Body)
	buf := new(bytes.Buffer)
	buf.ReadFrom(resp.Body)
	//s := buf.String()
	return string(content)
}

func GetIntranetIp() {
	addrs, err := net.InterfaceAddrs()

	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	for _, address := range addrs {
		// 检查ip地址判断是否回环地址
		if ipnet, ok := address.(*net.IPNet); ok && !ipnet.IP.IsLoopback() && ipnet.IP.To4() != nil {
			fmt.Println("ip:", ipnet.IP.String())
		}
	}
}

func inetNtoa(ipnr int64) net.IP {
	var dataBytes [4]byte
	dataBytes[0] = byte(ipnr & 0xFF)
	dataBytes[1] = byte((ipnr >> 8) & 0xFF)
	dataBytes[2] = byte((ipnr >> 16) & 0xFF)
	dataBytes[3] = byte((ipnr >> 24) & 0xFF)

	return net.IPv4(dataBytes[3], dataBytes[2], dataBytes[1], dataBytes[0])
}

func inetAton(ipnr net.IP) int64 {
	bits := strings.Split(ipnr.String(), ".")

	b0, _ := strconv.Atoi(bits[0])
	b1, _ := strconv.Atoi(bits[1])
	b2, _ := strconv.Atoi(bits[2])
	b3, _ := strconv.Atoi(bits[3])

	var sum int64

	sum += int64(b0) << 24
	sum += int64(b1) << 16
	sum += int64(b2) << 8
	sum += int64(b3)

	return sum
}

func IpBetween(from net.IP, to net.IP, test net.IP) bool {
	if from == nil || to == nil || test == nil {
		fmt.Println("An ip input is nil") // or return an error!?
		return false
	}

	from16 := from.To16()
	to16 := to.To16()
	test16 := test.To16()
	if from16 == nil || to16 == nil || test16 == nil {
		fmt.Println("An ip did not convert to a 16 byte") // or return an error!?
		return false
	}

	if bytes.Compare(test16, from16) >= 0 && bytes.Compare(test16, to16) <= 0 {
		return true
	}
	return false
}

func IsPublicIP(IP net.IP) bool {
	if IP.IsLoopback() || IP.IsLinkLocalMulticast() || IP.IsLinkLocalUnicast() {
		return false
	}
	if ip4 := IP.To4(); ip4 != nil {
		switch true {
		case ip4[0] == 10:
			return false
		case ip4[0] == 172 && ip4[1] >= 16 && ip4[1] <= 31:
			return false
		case ip4[0] == 192 && ip4[1] == 168:
			return false
		default:
			return true
		}
	}
	return false
}

func GetPublicIP() string {
	conn, _ := net.Dial("udp", "8.8.8.8:80")
	defer conn.Close()
	localAddr := conn.LocalAddr().String()
	idx := strings.LastIndex(localAddr, ":")
	return localAddr[0:idx]
}

func GetOutBoundIP() (ip string, err error) {
	conn, err := net.Dial("udp", "8.8.8.8:53")
	if err != nil {
		fmt.Println(err)
		return
	}
	localAddr := conn.LocalAddr().(*net.UDPAddr)

	ip = strings.Split(localAddr.String(), ":")[0]
	return
}

// 获取网卡Mac地址
func GetMacAddr() string {
	interfaces, err := net.Interfaces()
	if err != nil {
		panic("Poor soul, here is what you got: " + err.Error())
	}
	if len(interfaces) == 0 {
		return ""
	}

	maxIndexInterface := interfaces[0]
	for _, inter := range interfaces {
		if inter.HardwareAddr == nil {
			continue
		}
		if inter.Flags&net.FlagUp == 1 {
			maxIndexInterface = inter
		}
	}
	return maxIndexInterface.HardwareAddr.String()
}

func externalIP() (net.IP, error) {
	ifaces, err := net.Interfaces()
	if err != nil {
		return nil, err
	}
	for _, iface := range ifaces {
		if iface.Flags&net.FlagUp == 0 {
			continue // interface down
		}
		if iface.Flags&net.FlagLoopback != 0 {
			continue // loopback interface
		}
		addrs, err := iface.Addrs()
		if err != nil {
			return nil, err
		}
		for _, addr := range addrs {
			ip := getIpFromAddr(addr)
			if ip == nil {
				continue
			}
			return ip, nil
		}
	}
	return nil, errors.New("connected to the network")
}

func getIpFromAddr(addr net.Addr) net.IP {
	var ip net.IP
	switch v := addr.(type) {
	case *net.IPNet:
		ip = v.IP
	case *net.IPAddr:
		ip = v.IP
	}
	if ip == nil || ip.IsLoopback() {
		return nil
	}
	ip = ip.To4()
	if ip == nil {
		return nil // not an ipv4 address
	}
	return ip
}

// HasLocalIPAddr 检测 IP 地址字符串是否是内网地址
func HasLocalIPAddr(ip string) bool {
	return HasLocalIP(net.ParseIP(ip))
}

// HasLocalIP 检测 IP 地址是否是内网地址
// 通过直接对比ip段范围效率更高
func HasLocalIP(ip net.IP) bool {
	if ip.IsLoopback() {
		return true
	}

	ip4 := ip.To4()
	if ip4 == nil {
		return false
	}

	return ip4[0] == 10 || // 10.0.0.0/8
		(ip4[0] == 172 && ip4[1] >= 16 && ip4[1] <= 31) || // 172.16.0.0/12
		(ip4[0] == 169 && ip4[1] == 254) || // 169.254.0.0/16
		(ip4[0] == 192 && ip4[1] == 168) // 192.168.0.0/16
}

// ClientPublicIP 尽最大努力实现获取客户端公网 IP 的算法。
// 解析 X-Real-IP 和 X-Forwarded-For 以便于反向代理（nginx 或 haproxy）可以正常工作。
func ClientPublicIP(r *http.Request) string {
	var ip string
	for _, ip = range strings.Split(r.Header.Get("X-Forwarded-For"), ",") {
		if ip = strings.TrimSpace(ip); ip != "" && !HasLocalIPAddr(ip) {
			return ip
		}
	}

	if ip = strings.TrimSpace(r.Header.Get("X-Real-Ip")); ip != "" && !HasLocalIPAddr(ip) {
		return ip
	}

	if ip = RemoteIP(r); !HasLocalIPAddr(ip) {
		return ip
	}

	return ""
}

// RemoteIP 通过 RemoteAddr 获取 IP 地址， 只是一个快速解析方法。
func RemoteIP(r *http.Request) string {
	ip, _, _ := net.SplitHostPort(r.RemoteAddr)
	return ip
}
