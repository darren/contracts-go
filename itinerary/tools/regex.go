package tools

import "regexp"

// CheckIdCard 检验身份证
func CheckIdCard(card string) bool {
	regRuler := IdCardCheckRule()

	// 正则调用规则
	reg := regexp.MustCompile(regRuler)

	// 返回 MatchString 是否匹配
	return reg.MatchString(card)
}

// CheckMobile 检验手机号
func CheckMobile(phone string) bool {
	regRuler := PhoneCheckRule()

	// 正则调用规则
	reg := regexp.MustCompile(regRuler)

	// 返回 MatchString 是否匹配
	return reg.MatchString(phone)

}

// IdCardCheckRule 身份证验证规则
func IdCardCheckRule() string {
	//18位身份证 ^(\d{17})([0-9]|X)$
	// 匹配规则
	// (^\d{15}$) 15位身份证
	// (^\d{18}$) 18位身份证
	// (^\d{17}(\d|X|x)$) 18位身份证 最后一位为X的用户
	return "(^\\d{15}$)|(^\\d{18}$)|(^\\d{17}(\\d|X|x)$)"
}

// PhoneCheckRule 手机号验证规则
func PhoneCheckRule() string {
	// 匹配规则
	// ^1第一位为一
	// [345789]{1} 后接一位345789 的数字
	// \\d \d的转义 表示数字 {9} 接9位
	// $ 结束符
	return "^1[3|4|5|6|7|8|9][0-9]\\d{9}$"
}

// EmailCheckRule 邮箱验证规则
func EmailCheckRule() string {
	return "^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$"
}
